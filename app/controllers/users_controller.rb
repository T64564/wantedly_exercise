class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update]
  before_action :correct_user, only: [:edit, :update]

  def index
    @users = User.paginate(page: params[:page])
  end

  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
    @users_skill = @user.users_skill.order("n_liked desc")
  end

  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      flash[:success] = "Welcome to the Sample App!"
      redirect_to @user
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      # 更新に成功したときの処理
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def add_skill
    @user = User.find(params[:user_id])
    skill = Skill.find_or_create_by(name: params[:add_skill])
    UsersSkill.find_or_create_by(user_id: @user.id, skill_id: skill.id)
    redirect_to @user
  end

  def like_skill
    @user = User.find(params[:user_id])
    if @user.id == current_user.id
      # 自分のページにいいねは出来ない
      flash[:danger] = "Cannot like a skill on your profile"
    else
      @skill = Skill.find(params[:skill_id])
      users_skill = UsersSkill.find_by(user_id: @user.id, skill_id: @skill.id)
      usl = UsersSkillLike.new(users_skill_id: users_skill.id, user_id: current_user.id)
      if usl.save
        # まだlikeしていない
        flash[:success] = "Liked skill!"
        users_skill.update(n_liked: users_skill.n_liked + 1)
      else
        # 既にlikeした
        flash[:success] = "Canceled like"
        UsersSkillLike.find_by(users_skill_id: users_skill.id, user_id: current_user.id).destroy
        users_skill.update(n_liked: users_skill.n_liked - 1)
      end
    end
    redirect_to @user
  end

  def hide_skill
    @user = User.find(params[:user_id])
    users_skill = UsersSkill.find(params[:users_skill_id])
    if @user.id == current_user.id
      users_skill.update(visible: !users_skill.visible)
    end
    if users_skill.visible
      flash[:success] = "Show #{ Skill.find_by(id: users_skill.skill_id).name }"
    else
      flash[:success] = "Hide #{ Skill.find_by(id: users_skill.skill_id).name }"
    end
    redirect_to @user
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation)
  end

  # beforeフィルター
  # ログイン済みユーザーかどうか確認
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end

  # 正しいユーザーかどうか確認
  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end
end
