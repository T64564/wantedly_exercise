class SkillsController < ApplicationController

  def show
    @skill = Skill.find(params[:id])
    user_ids = []
    users_skills = UsersSkill.where(skill_id: @skill.id, visible: true)
    users_skills.each do |us|
      user_ids.push(us.user_id)
    end
    @users = User.where(id: user_ids).paginate(page: params[:page])
    @skills = Skill.paginate(page: params[:page])
  end
end
