class UsersSkillLike < ActiveRecord::Base
  belongs_to :users_skill
  belongs_to :user
  validates :users_skill_id, presence: true
  validates :user_id, presence: true
  validates :users_skill_id,  uniqueness: { scope: [:users_skill_id, :user_id] }
end
