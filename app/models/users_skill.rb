class UsersSkill < ActiveRecord::Base
  belongs_to :user
  belongs_to :skill
  has_many :users_skill_like
  validates :user_id, presence: true
  validates :user_id,  uniqueness: { scope: :skill_id }
  validates :skill_id, presence: true

  def like_users(user_id)
    usls = UsersSkillLike.where(users_skill_id: id)
    like_user_ids = []
    usls.each do |usl|
      like_user_ids.push(usl.user_id)
    end
    User.where(id: like_user_ids).limit(10)
  end
end
