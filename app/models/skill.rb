class Skill < ActiveRecord::Base
  has_many :users_skill
  validates :name, presence: true, length: { maximum: 50 },
    uniqueness: true

  def self.findOrCreate(name)
    skill = Skill.find_by(name: name)
    if skill.nil?
      skill = Skill.!create(name: name)
    end
    return skill
  end
end
