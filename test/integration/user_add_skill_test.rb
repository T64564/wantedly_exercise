require 'test_helper'

class UserAddSkillTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @skill = skills(:ruby)
  end

  test "fail to add new skill" do
    log_in_as(@user)
    assert is_logged_in?
    get user_path(@user)
    prev = Skill.count
    post add_skill_path(@user), add_skill: 'hoge', user_id: @user.id
    assert_equal prev + 1, Skill.count
  end

  test "fail to add skill" do
    log_in_as(@user)
    assert is_logged_in?
    get user_path(@user)
    prev = Skill.count
    post add_skill_path(@user), add_skill: @skill.name, user_id: @user.id
    assert_equal prev, Skill.count
  end
end
