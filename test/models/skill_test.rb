require 'test_helper'

class SkillTest < ActiveSupport::TestCase

  def setup
    @skill = skills(:ruby)
  end

  test "name should be present" do
    @skill.name = "     "
    assert_not @skill.valid?
  end
end
