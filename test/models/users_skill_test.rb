require 'test_helper'

class UsersSkillTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @skill = skills(:ruby)
    @users_skill = @user.users_skill.build(skill_id: @skill.id)
  end

  test "should be valid" do
    assert @users_skill.valid?
  end

  test "user id should be present" do
    @users_skill.user_id = nil
    assert_not @users_skill.valid?
  end

  test "combination of [user id, skill_id] should be present" do
    d = @users_skill.dup
    d.user_id = @user.id
    d.skill_id = @skill.id
    @user.save
    assert_not d.valid?
  end
end
