require 'test_helper'

class UsersSkillLikeTest < ActiveSupport::TestCase
  def setup
    @users_skill = users_skills(:one)
    @user = users(:archer)
    @usl = @users_skill.users_skill_like.build(user_id: @user.id)
  end

  test "combination of [users_skill id, user id] should be present" do
    d = @usl.dup
    d.users_skill_id = @users_skill.id
    d.user_id = @user.id
    @usl.save
    assert_not d.valid?
  end
end
