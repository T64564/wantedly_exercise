Rails.application.routes.draw do

  root 'home#index'
  get 'signup' => 'users#new'
  get 'sessions/new'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'

  resources :users
  post 'add_skill' => 'users#add_skill'
  post 'like_skill' => 'users#like_skill'
  post 'hide_skill' => 'users#hide_skill'
  resources :skills
end
