# Wantedly Web 開発 課題

## テーマ
「Wantedly プロフィールページの「スキル・特徴」の再現」

## URL
https://frozen-reef-22052.herokuapp.com/  
[ログインページ](https://frozen-reef-22052.herokuapp.com/login)でEmailに"a1@a.com"，Passwordに"password"を入力するとログインできます．20人のユーザが登録されています．それぞれのEmailは"a1@a.com" ~ "a20@a.com"，Passwordは全て"password"です．

## 使用フレームワーク
Ruby on Rails(v.4.2.2)

## 仕様
### 必須
* スキルは、自分に追加することができ、他人に追加することもできる。
* 他の人のプロフィールページで、スキルに+1することができる。(つけられる人、つける人、スキルの種類)でユニークとなる。
* プロフィールでは+1された回数が表示されていて、+1が多い順に表示されている。

### オプショナルのうち達成したもの
* +1の回数が多い上位6件は、それぞれ最大10人まで+1した人が表示されている
* 他の人につけた+1をキャンセルすることができる
* 他の人が自分につけたスキルを隠すことができる
* 同じスキルが付いている人の一覧を見ることができる

### オプショナルのうち達成していないもの
* JavaScriptでページの再読み込みの発生しないUIをつくる
* スキルの追加時にautocompleteでスキル名が補完されるUI
* N+1問題が発生しないようにしてください

## プロフィールページ
* ナビゲーションバーのAccount -> Profile で自分のプロフィールページに行くことが出来ます．
![自分のプロフィールページ](https://bytebucket.org/T64564/wantedly_excercise/raw/996f711894ca16d141fdee53c776b0a4fd3b37e8/.images_for_readme/Wantedly_excercise.001.png?token=8f4098f7d62fa4ba2a6557160ad49f74a73d2760)  
* ナビゲーションバーのUsersからユーザ一覧ページに行くことが出来ます．そこから好きなユーザのプロフィールページに行くことが出来ます．
![他ユーザのプロフィールページ](https://bytebucket.org/T64564/wantedly_excercise/raw/996f711894ca16d141fdee53c776b0a4fd3b37e8/.images_for_readme/Wantedly_excercise.002.png?token=f75031fbd9c9ce4efb4e3234262adf2ec5cc8660)
