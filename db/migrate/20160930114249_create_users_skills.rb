class CreateUsersSkills < ActiveRecord::Migration
  def change
    create_table :users_skills do |t|
      t.integer :user_id
      t.integer :skill_id
      t.boolean :visible, default: true

      t.timestamps null: false
    end
    add_foreign_key :user_id, :users
    add_foreign_key :skill_id, :skills
  end
end
