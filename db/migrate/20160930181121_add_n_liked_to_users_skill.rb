class AddNLikedToUsersSkill < ActiveRecord::Migration
  def change
    add_column :users_skills, :n_liked, :integer, default: 0
  end
end
