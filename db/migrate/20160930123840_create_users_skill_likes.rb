class CreateUsersSkillLikes < ActiveRecord::Migration
  def change
    create_table :users_skill_likes do |t|
      t.integer :users_skill_id
      t.integer :user_id

      t.timestamps null: false
    end
    add_foreign_key :users_skill_id, :users_skill
    add_foreign_key :user_id, :users
  end
end
