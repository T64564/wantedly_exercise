20.times do |i|
  name  = Faker::Name.name
  email = "a#{i+1}@a.com"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

skills = %W(Ruby PHP Java C C++ R Python リーダータイプ ガッツがある Webデザイン モバイルアプリデザイン)
skills.each do |s|
  Skill.create!(name: s)
end

20.times do |i|
  skills.length.times do |j|
    if rand(2) == 1
      us = UsersSkill.create!(user_id: i + 1, skill_id: j + 1)

      20.times do |k|
        if rand(2) == 1 &&  i != k
          UsersSkillLike.create!(users_skill_id: us.id, user_id: k + 1)
          us.update(n_liked: us.n_liked + 1)
        end
      end
    end
  end
end
